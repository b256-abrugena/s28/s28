//For item number 3:

db.users.insertOne(
{
	"name": "single",
	"accommodates": "2",
	"price": "1000",
	"description": "A single room with all the basic necessities",
	"rooms_available": "10",
	"isAvailable": "false"
}
)

//For item number 4: 

db.users.insertMany({
	"name": "double",
	"accommodates": "3",
	"price": "2000",
	"description": "A room fit for a small family going on a vacation",
	"rooms_available": "5",
	"isAvailable": "false"
},
{
	"name": "queen",
	"accommodates": "4",
	"price": "4000",
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"rooms_available": "15",
	"isAvailable": "false"
})

//For item number 5:
db.users.find({"name": "double"})

//For item number 6:
db.users.updateOne(
	{"_id": ObjectId("641a310bf5446748029dab44")},
	{$set: {"rooms_available": "0"}}
)

//For item number 7:
db.users.deleteMany({"rooms_available": "0"})